package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"github.com/gosuri/uiprogress"
)

var (
	context     = PARA_CONTEXT_PURPORT
	lineNum     = 0
	totalVerses = 8835
	sb          = &SB{
		VerseHeaders: make(map[string]int),
		Cantos:       []Canto{},
	}
	bar = uiprogress.AddBar(totalVerses)
)

func init() {
	bar.PrependFunc(func(b *uiprogress.Bar) string {
		return fmt.Sprintf("%-15v", sb.ctc)
	})
	bar.PrependCompleted()
	bar.AppendElapsed()
	uiprogress.Start()
}

func main() {
	file, err := os.Open("raw/sbepub.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	sb.r = bufio.NewReader(file)
	l := ""

	for {
		l = sb.readLine()
		switch l {
		case CANTO_HEADER:
			sb.addCanto()
		case CHAPTER_HEADER:
			sb.addChapter()
		case CANTO_SUMMARY:
			context = PARA_CONTEXT_CANTO_SUMMARY
		case VERSE_NUMBER_HEADING_HEADER:
			l = sb.getVerseRangeCode()
			switch {
			case sb.isSummaryHeader(l):
				context = PARA_CONTEXT_CHAPTER_SUMMARY
			case sb.isTextHeader(l):
				sb.getVST(l)
			default:
				sb.setFatalLineHeader(l)
			}
		case NORMAL_LEVEL,
			AFTER_VERSE,
			INDENTED,
			VERSE_IN_PURP,
			PROSE_VERSE_IN_PURP,
			THUS_ENDS,
			VERSE_REF:
			sb.addPara(l)
		}
	}
}
