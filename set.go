package main

import "log"

func (sb *SB) setFatalLineHeader(l string) {
	sb.VerseHeaders[l] = sb.lineNum
	log.Fatalf("Unexpected line after VERSE_NUMBER_HEADING_HEADER %s %d", l, sb.lineNum)
}

func (sb *SB) setVerseLine(l string) {
	lineSoFar := sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses[sb.vri].Roman
	if lineSoFar == "" {
		sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses[sb.vri].Roman += l
	} else {
		sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses[sb.vri].Roman += "\n" + l
	}
}

func (sb *SB) setProse() {
	sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses[sb.vri].Prose = true
}

func (sb *SB) setUvacaLine(l string) {
	sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses[sb.vri].UvacaLine = sb.readUntilEquals(COSTIGAN)
}

func (sb *SB) setSynonyms(l string) {
	if matchLine(l, COSTIGAN) {
		sb.setFatalLineHeader(l)
	}
	sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Synonyms = l
}

func (sb *SB) setTranslation(l string) {
	if matchLine(l, COSTIGAN) {
		sb.setFatalLineHeader(l)
	}
	sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Translation = l
}
