package main

import (
	"log"
	"regexp"
	"strconv"
	"strings"
)

func (sb *SB) addCanto() {
	sb.Cantos = append(sb.Cantos, Canto{})

	matches := regexp.MustCompile(CANTO_NAME_REGEX).
		FindStringSubmatch(sb.readUntilEquals(COSTIGAN))
	cantoName := matches[2]
	cantoNumber, err := strconv.Atoi(matches[1])
	if err != nil {
		log.Fatalf("Error in canto num: %v", err)
	}

	sb.cni = len(sb.Cantos) - 1
	sb.chi = 0
	sb.txi = 0
	sb.vri = 0

	sb.Cantos[sb.cni].CantoName = cantoName
	sb.Cantos[sb.cni].CantoNumber = cantoNumber
}

func (sb *SB) addChapter() {

	sb.Cantos[sb.cni].Chapters = append(sb.Cantos[sb.cni].Chapters, Chapter{})

	matches := regexp.MustCompile(CHAPTER_NAME_REGEX).
		FindStringSubmatch(sb.readUntilEquals(COSTIGAN))
	chapterName := matches[2]
	chapterNumber, err := strconv.Atoi(matches[1])
	if err != nil {
		log.Fatalf("Error in canto num: %v", err)
	}

	sb.chi = len(sb.Cantos[sb.cni].Chapters) - 1
	sb.txi = 0
	sb.vri = 0

	sb.Cantos[sb.cni].Chapters[sb.chi].ChapterName = chapterName
	sb.Cantos[sb.cni].Chapters[sb.chi].ChapterNumber = chapterNumber
}

func (sb *SB) addText(l string) {
	sb.Cantos[sb.cni].Chapters[sb.chi].Texts =
		append(sb.Cantos[sb.cni].Chapters[sb.chi].Texts, Text{})

	matches := regexp.MustCompile(sb.getExpectedTextCodeRegex()).FindStringSubmatch(l)

	sb.txi = len(sb.Cantos[sb.cni].Chapters[sb.chi].Texts) - 1

	sb.vri = 0

	sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].TextCode = matches[0]

	ivn, err := strconv.Atoi(matches[1])
	jvn := ivn
	if err != nil {
		log.Fatalf("Error in i verse num: %v", err)
	}
	if matches[3] != "" {
		jvn, err = strconv.Atoi(matches[3])
		if err != nil {
			log.Fatalf("Error in i verse num: %v", err)
		}
	}

	sb.pivn = sb.civn
	sb.civn = ivn
	sb.pjvn = sb.cjvn
	sb.cjvn = jvn
	sb.ptc = sb.ctc
	sb.ctc = matches[0]
	sb.totalVerses += (jvn - ivn) + 1

	if sb.pjvn != sb.civn-1 && (sb.civn != 1 && sb.civn != 0) {
		log.Fatalf("break %s %s %d", sb.ptc, sb.ctc, sb.lineNum)
	}

	for i := ivn; i <= jvn; i++ {
		bar.Incr()
		sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses =
			append(sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses,
				Verse{
					VerseNumber: i,
				})
	}
}

func (sb *SB) addPara(l string) {
	para := sb.readUntilEquals(COSTIGAN)
	if l == VERSE_IN_PURP {
		para = strings.ReplaceAll(para, `\`, `\n`)
	}
	switch context {
	case PARA_CONTEXT_PURPORT:
		sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Purport =
			append(sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Purport, Paragraph{
				Type: getParaType(l),
				Para: para,
			})
	case PARA_CONTEXT_CHAPTER_SUMMARY:
		sb.Cantos[sb.cni].Chapters[sb.chi].Summary =
			append(sb.Cantos[sb.cni].Chapters[sb.chi].Summary, Paragraph{
				Type: getParaType(l),
				Para: para,
			})
	}
}
