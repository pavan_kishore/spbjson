package main

import (
	"fmt"
	"strings"
)

// get verse cum synonyms cum translation
func (sb *SB) getVST(l string) {
	sb.addText(l)
	for {
		l = sb.readLine()
		if l == "" {
			continue
		}
		switch l {
		case UVACA_LINE:
			sb.setUvacaLine(l)
		case PROSE_VERSE_HEADER:
			sb.setProse()
			fallthrough
		case VERSE_TEXT_HEADER,
			ONE_LINE_VERSE_HEADER:
			sb.setVerseLine(sb.readUntilEquals(COSTIGAN))
		case NEXT_VERSE_CALIBRE2_HEADER:
			if len(sb.getCurrentText().Verses) > sb.vri+1 {
				sb.vri++
			}
		case SYNONYMS_HEADER:
			sb.setSynonyms(sb.readUntilEquals(COSTIGAN))
		case TRANSLATION_HEADER:
			sb.setTranslation(sb.readUntilEquals(COSTIGAN))
			context = PARA_CONTEXT_PURPORT
			return
		default:
			if !matchLine(l, IGNORE_LINES) {
				sb.setFatalLineHeader(l)
			}
		}
	}
}

func (sb *SB) getVerseRangeCode() string {
	l := sb.readUntilEquals(COSTIGAN)
	temp := strings.Split(l, ",")
	return strings.Trim(temp[len(temp)-1], " ")
}

func (sb *SB) getCurrentCanto() Canto {
	return sb.Cantos[sb.cni]
}

func (sb *SB) getCurrentChapter() Chapter {
	return sb.Cantos[sb.cni].Chapters[sb.chi]
}

func (sb *SB) getCurrentText() Text {
	return sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi]
}

func (sb *SB) getCurrentVerse() Verse {
	return sb.Cantos[sb.cni].Chapters[sb.chi].Texts[sb.txi].Verses[sb.vri]
}

func (sb *SB) getExpectedTextCodeRegex() string {
	return fmt.Sprintf(TEXT_CODE_PATTERN,
		sb.getCurrentCanto().CantoNumber,
		sb.getCurrentChapter().ChapterNumber)
}

func getParaType(l string) string {
	switch l {
	case NORMAL_LEVEL, INDENTED:
		return "normal"
	case AFTER_VERSE:
		return "afterVerse"
	case PROSE_VERSE_IN_PURP:
		return "prose"
	case THUS_ENDS:
		return "end"
	case VERSE_IN_PURP:
		return "verse"
	case VERSE_REF:
		return "ref"
	default:
		return l
	}
}
