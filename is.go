package main

import "fmt"

func (sb *SB) isSummaryHeader(l string) bool {
	return l == fmt.Sprintf(CHAPTER_SUMMARY_PATTERN,
		sb.getCurrentCanto().CantoNumber,
		sb.getCurrentChapter().ChapterNumber)
}

func (sb *SB) isTextHeader(l string) bool {
	return matchLine(l, sb.getExpectedTextCodeRegex())
}
