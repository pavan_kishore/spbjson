package main

import "bufio"

type SB struct {
	Cantos []Canto `json:"cantos"`

	r                                          *bufio.Reader
	cni, chi, txi, vri, pivn, pjvn, civn, cjvn int
	lineNum, totalVerses                       int
	ptc, ctc                                   string
	VerseHeaders                               map[string]int `json:"verseHeaders"`
}

type Canto struct {
	CantoName   string    `json:"cantoName"`
	CantoNumber int       `json:"cantoNumber"`
	Chapters    []Chapter `json:"chapters"`
}

type Chapter struct {
	ChapterName   string      `json:"chapterName"`
	ChapterNumber int         `json:"chapterNumber"`
	Summary       []Paragraph `json:"summary"`
	Texts         []Text      `json:"texts"`
}

type Text struct {
	TextCode    string      `json:"textCode"`
	Verses      []Verse     `json:"verses"`
	Synonyms    string      `json:"synonyms"`
	Translation string      `json:"translation"`
	Purport     []Paragraph `json:"purport,omitempty"`
}

type Verse struct {
	VerseNumber int    `json:"verseNumber"`
	Roman       string `json:"roman"`
	Sanskrit    string `json:"sanskrit,omitempty"`
	UvacaLine   string `json:"uvacaLine,omitempty"`
	Prose       bool   `json:"prose,omitempty"`
}

type Paragraph struct {
	Type string `json:"type"`
	Para string `json:"para"`
}
