package main

import (
	"regexp"
	"strings"
)

func escapedown(l string) string {
	l = strings.Trim(l, " ")
	l = strings.ReplaceAll(l, "\\'", "'")
	l = strings.ReplaceAll(l, "\\\"", "\"")
	l = strings.ReplaceAll(l, "\\\\[", "[")
	l = strings.ReplaceAll(l, "\\\\]", "]")
	l = markdown(l)
	return l
}

func markdown(l string) string {
	matches := regexp.MustCompile(ITALIC_REGEX).FindAllString(l, -1)
	for _, match := range matches {
		l = strings.ReplaceAll(l, match,
			strings.ReplaceAll(
				strings.ReplaceAll(
					strings.ReplaceAll(match, ITALIC, ""), "[", "_"), "]", "_"))
	}

	return l
}

func matchLine(l string, reg string) bool {
	m, _ := regexp.MatchString(reg, l)
	return m
}
