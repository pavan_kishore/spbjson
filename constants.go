package main

const (
	PARA_CONTEXT_PURPORT         = 0
	PARA_CONTEXT_CHAPTER_SUMMARY = 1
	PARA_CONTEXT_CANTO_SUMMARY   = 2
)

const (
	CANTO_HEADER                = "::: {.Canto}"
	CANTO_NAME_REGEX            = "Canto (\\d{1,2}): \"(.+)\""
	CHAPTER_HEADER              = "::: {.Chapter}"
	CHAPTER_NAME_REGEX          = "SB \\d{1,2}\\.(\\d{1,3}): (.+)"
	VERSE_NUMBER_HEADING_HEADER = "::: {.Verse-Number-Heading}"
	CHAPTER_SUMMARY_PATTERN     = "SB %d.%d Summary"
	TEXT_CODE_PATTERN           = "SB %d.%d.(\\d{1,3})(-(\\d{1,3})|)"
	ONE_LINE_VERSE_HEADER       = "::: {.One-line-verse}"
	PROSE_VERSE_HEADER          = "::: {.Prose-Verse}"
	UVACA_LINE                  = "::: {.Uvaca-line}"
	NEXT_VERSE_CALIBRE2_HEADER  = "::: {.calibre2}"
	VERSE_TEXT_HEADER           = "::: {.Verse-Text}"
	SYNONYMS_HEADER             = "::: {.Synonyms}"
	TRANSLATION_HEADER          = "::: {.Translation}"
	VERSE_SECTION_HEADER        = "::: {.Verse-Section}"
	NORMAL_LEVEL                = "::: {.Normal-Level}"
	AFTER_VERSE                 = "::: {.After-Verse}"
	INDENTED                    = "::: {.Indented}"
	PROSE_VERSE_IN_PURP         = "::: {.Prose-Verse-in-purp}"
	THUS_ENDS                   = "::: {.Thus-end}"
	VERSE_IN_PURP               = "::: {.Verse-in-purp}"
	VERSE_REF                   = "::: {.VerseRef}"
	PURPORT_HEADER              = "PURPORT"
	CANTO_SUMMARY               = "::: {.CantoSummary}"
	COSTIGAN                    = ":::"
	ITALIC                      = "{.Italic}"
	ITALIC_REGEX                = `\[.*?\]\{\.Italic\}`
	IGNORE_LINES                = "(::: \\{\\.calibre6\\})|(INVOCATION$)|(TEXTS \\d{1,4}([a-z]|)-(-|)\\d{1,4}([a-z]|)$)|(TRANSLATION$)|(::: \\{\\.Verse-Section\\}$)|(SYNONYMS$)|(::: \\{\\.Synonyms-Section\\}$)|(::: \\{\\.Textnum\\}$)|(TEXT \\d{1,4}([a-z]|)$)|(::: \\{\\.Book\\}$)|(TEXT$)|(:::$)"

	SB_JSON_PATH = "res/sb.json"
)
