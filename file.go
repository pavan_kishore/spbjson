package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/gosuri/uiprogress"
)

func (sb *SB) readUntilMatch(match string) string {
	content := ""
	l := sb.readLine()
	for !matchLine(l, match) {
		content += l + " "
		l = sb.readLine()
	}

	content = escapedown(content)
	if strings.Contains(content, "{.Italic}") {
		log.Fatal(sb.lineNum)
	}
	return content
}

func (sb *SB) readUntilEquals(match string) string {
	content := ""
	l := sb.readLine()
	for l != match {
		content += l + " "
		l = sb.readLine()
	}

	content = escapedown(content)
	if strings.Contains(content, "{.Italic}") {
		log.Fatal(sb.lineNum)
	}
	return content
}

func (sb *SB) readLine() string {
	b, _, err := sb.r.ReadLine()
	if err == io.EOF {
		uiprogress.Stop()
		log.Println("End of file")
		data, err := json.MarshalIndent(sb, "", "  ")
		if err != nil {
			log.Fatal("Cannot marshal json: ", err)
		}

		log.Println("Total Verses:", sb.totalVerses)
		if err = ioutil.WriteFile(SB_JSON_PATH, data, 0644); err != nil {
			log.Fatal("Cant write to file:", err)
		}
		os.Exit(0)
	}
	sb.lineNum++

	return string(b)
}
